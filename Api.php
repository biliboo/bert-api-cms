<?php

namespace Bert\ApiCms;

use Bert\ApiCore\Api as CoreApi;

class Api extends CoreApi
{
    /**
     * @param string $tag
     * @param string $locale
     * @return array
     */
    public function getArticle($tag, $locale)
    {
        $item = $this->cache->getItem($this->getArticleCacheKey($tag, $locale));

        if ($item->isHit()) {
            return $item->get();
        }

        $response = $this->internally(function() use ($tag, $locale) {
            return $this->get(sprintf('/article/%s?l=%s', base64_encode($tag), $locale));
        });

        if (self::HTTP_OK === $this->lastHttpCode) {
            $item->set($response);
            $this->cache->save($item);
            return $response;
        } else if (self::HTTP_NOT_FOUND === $this->lastHttpCode) {
            $item->set(false);
            $this->cache->save($item);
        }

        return false;
    }

    /**
     * @param string $tag
     * @param string $locale
     * @return string
     */
    private function getArticleCacheKey($tag, $locale)
    {
        $key = sprintf('cms.article.%s.%s', strtolower($tag), strtolower($locale));

        return str_replace(['{', '}', '(', ')', '/', '\\', '@', ':'], '-', $key);
    }

    /**
     * @param string $tag
     * @param string $locale
     */
    public function invalidateArticle($tag, $locale)
    {
        $this->cache->deleteItem($this->getArticleCacheKey($tag, $locale));
    }
}
